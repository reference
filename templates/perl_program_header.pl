#! /usr/bin/perl
# This program is part of da_reference, and is released under the
# terms of the GPL version 2, or any later version, at your
# option. See the file README and COPYING for more information.
# Copyright 2004 by Don Armstrong <don@donarmstrong.com>.
# $Id: perl_program_header.pl 30 2004-06-29 10:26:20Z don $


use warnings;
use strict;


use Getopt::Long;
use Pod::Usage;

=head1 NAME

foo - 

=head1 SYNOPSIS

foo [options] 

 Options:
  --debug, -d debugging level (Default 0)
  --help,-h display this help
  --man,-m display manual

=head1 OPTIONS

=over

=item B<--debug, -d>

Debug verbosity. (Default 0)

=item B<--help, -h>

Display brief useage information.

=item B<--man, -m>

Display this manual.

=back

=head1 EXAMPLES


=cut



use vars qw($DEBUG);

my %options = (debug  => 0,
	       help   => 0,
	       man    => 0,
	      );

GetOptions(\%options,'debug|d','help|h','man|m');

pod2usage() if $options{help};
pod2usage({verbose=>2}) if $options{man};

$DEBUG = $options{debug};



__END__
