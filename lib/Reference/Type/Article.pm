# This module is part of , and is released
# under the terms of the GPL version 2, or any later version. See the
# file README and COPYING for more information.
# Copyright 2003 by Don Armstrong <don@donarmstrong.com>.
# $Id: Article.pm 30 2004-06-29 10:26:20Z don $

package Reference::Type::Article;

=head1 NAME

Reference::Type::Article -- Article reference type

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 BUGS

None known.

=cut


use strict;
use vars qw($VERSION $DEBUG);
use Carp;

use base qw(Reference Reference::Field::Author Reference::Field::Pages Reference::Field::Journal Reference::Field::Date);

use NEXT;
use Reference;
use Params::Validate qw(:types validate_with);

BEGIN{
     ($VERSION) = q$LastChangedRevision: 30 $ =~ /\$LastChangedRevision:\s+([^\s+])/;
     $DEBUG = 0 unless defined $DEBUG;
}


=head2 name

=head3 Usage

     $article->name($article_name);
     my $article_name = $article->name;

=head3 Function

Returns the article name if it has been set, or builds an article name
from the author, journal, volume, and page if none is set.

=cut

sub name{
     my $self = shift;
     my %params;
     if (scalar(@_) == 1) {
	  $params{name} = shift;
     }
     else {
	  %params = validate_with(params => \@_,
				  spec   => {name => {type => SCALAR,
						      optional => 1,
						     },
					     output => {type    => SCALAR,
							default => 'scalar',
						       },
					    },
				 );
     }

     if (defined $params{name}) {
	  $self->{reference}->{name} = $params{name};
	  return $params{name};
     }
     if (not defined $self->{reference}->{name}) {
	  my ($name) = $self->first_author =~ /(\w+)$/;
	  if (not defined $name) {
	       no warnings qw(uninitialized);
	       $name = $self->journal . $self->volume . $self->pages;
	  }
	  $name .= $self->year if defined $self->year;
	  $self->{reference}->{name} = $name;
	  return $name;
     }
     else {
	  return $self->{reference}->{name};
     }
}

=head2 ref_fields

=head3 Usage

     my @ref_fields = $self->ref_fields;

=head3 Returns

Returns the list of reference fields which this type of reference
supports.

=cut

sub ref_fields($){
     my $self = shift;

     return qw(author title year abstract journal pmid medline_id volume date number pages keywords doi html pdf month);
}


=head2 _init

=head3 Usage

Called by Reference's new function

=head3 Function

Call superclass's _init function [C<$self->NEXT::_init>], set up the
bibtex_mapping and bibtex_order.

=cut

sub _init($){
     my $self = shift;

     $self->NEXT::_init;
     $self->{type} = 'article';
#     $self->{bibtex_mapping} = {Article  => 'name',
#				author   => 'author',
#				title    => 'title',
#				journal  => 'journal',
#				year     => 'year',
#				key      => 'keywords',
#				volume	 => 'volume',
#				number	 => 'number',
#				pages	 => 'pages',
#				month	 => 'month',
#				abstract => 'abstract',
#				pmid	 => 'pmid',
#				mlid     => 'medline_id',
#				# doi	 => 'doi',
#				# html	 => 'html',
#				# pdf    => 'pdf',
#			       };
#     $self->{bibtex_order} = [qw(Article author title journal
#				 year key volume number pages
#				 month abstract pmid mlid doi
#				 html pdf),];
}




1;


__END__






