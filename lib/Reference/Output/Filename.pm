# This module is part of da_reference, and is released under the terms
# of the GPL version 2, or any later version, at your option. See the
# file README and COPYING for more information.
# Copyright 2009 by Don Armstrong <don@donarmstrong.com>.
# $Id: Filename.pm 43 2009-03-20 06:33:14Z don $

package Reference::Output::Filename;

=head1 NAME

Reference::Output::Filename -- Output a filename for the reference

=head1 SYNOPSIS

     print filename($reference);

Returns a filename for the reference

=head1 BUGS

None known.

=cut


use strict;
use vars qw($REVISION $DEBUG @EXPORT @EXPORT_OK %EXPORT_TAGS);

use base qw(Exporter);

BEGIN{
     ($REVISION) = q$LastChangedRevision: 36 $ =~ /\$LastChangedRevision:\s+([^\s+])/;
     $DEBUG = 0 unless defined $DEBUG;

     @EXPORT = qw(filename);
     @EXPORT_OK = qw();
     %EXPORT_TAGS = (output => [qw(filename)],
		    );
     Exporter::export_ok_tags(qw(output));
     $EXPORT_TAGS{all} = [@EXPORT_OK];

}

# Assigned and discussed at the end of this file
my %bibtex_mapping;

use Carp;


=head2 filename

     print filename($reference).'.pdf';

Returns a filename for a reference

=cut

sub filename{
     my $reference = shift;

     my $title = eval { $reference->title(); };
     my $fauthor = eval { $reference->first_author(output=>'last'); };
     my $cauthor = eval { $reference->corresponding_author(output=>'last');};
     if (defined $fauthor and defined $cauthor and $fauthor eq $cauthor) {
	 $fauthor = undef;
     }
     my $journal = eval { $reference->journal(output =>'bibtex');};
     my $volume = eval {$reference->volume();};
     my $number = eval {$reference->number();};
     my $page = eval{$reference->pages(output => 'bibtex');};
     $page =~ s/\s*--\s*\d+\s*// if defined $page;
     my $year = eval{$reference->date(output=>'year');};
     my $pmid = eval{$reference->pmid();};

     return join('_',
		 map {s/\W+/_/g; $_} map{defined $_ ?$_:()}
		 ($title,$fauthor,$cauthor,
		  $journal,$volume,$number,$page,$year,defined $pmid?"pmid_$pmid":undef));


 }

1;


__END__






