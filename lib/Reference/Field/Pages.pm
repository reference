# This module is part of da_reference, and is released
# under the terms of the GPL version 2, or any later version. See the
# file README and COPYING for more information.
# Copyright 2003 by Don Armstrong <don@donarmstrong.com>.
# $Id: Pages.pm 45 2013-09-10 18:05:31Z don $

package Reference::Field::Pages;

=head1 NAME

 --

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 BUGS

None known.

=cut


use strict;
use vars qw($REVISION $DEBUG);

use NEXT;
use Params::Validate qw(:types validate_with);

BEGIN{
     ($REVISION) = q$LastChangedRevision: 45 $ =~ /\$LastChangedRevision:\s+([^\s+])/;
     $DEBUG = 0 unless defined $DEBUG;
}


sub _init{
     my $self = shift;

     $self->{reference}->{pages} = {start => undef,
				    stop  => undef,
				   };

     $self->NEXT::_init;

}

sub pages{
     my $self = shift;
     my %params;
     if (scalar(@_) == 1) {
	  $params{pages} = shift;
	  $params{output} = 'scalar';
     }
     else {
	  %params = validate_with(params => \@_,
				  spec   => {pages => {type     => ARRAYREF|SCALAR|HASHREF,
						       optional => 1,
						      },
					     start => {type => SCALAR,
						       optional => 1,
						      },
					     stop  => {type => SCALAR,
						       optional => 1,
						      },
					     output => {default => 'scalar',
							type    => SCALAR,
						       },
					    },
				 );
     }
     # Update author according to the passed information
     if (defined $params{start} or defined $params{stop}) {
	  $self->{reference}->{pages}->{start} = $params{start} if defined $params{start};
	  $self->{reference}->{pages}->{stop}  = $params{stop} if defined $params{stop};
     }
     elsif (defined $params{pages}) {
	  $self->{reference}->{pages} = {start => undef,
					 stop  => undef,
					};
	  ($self->{reference}->{pages}->{start},$self->{reference}->{pages}->{stop}) = split(/\-+/,$params{pages});
     }

     if (wantarray) {
	  return grep {defined} ($self->{reference}->{pages}->{start},$self->{reference}->{pages}->{stop});
     }
     local $_ = $params{output};
     if (/bibtex/) {
	  return join('--',map {defined $_ ? $_ : ()} ($self->{reference}->{pages}->{start},$self->{reference}->{pages}->{stop}));
     }
     else {
	  return join('-',map {defined $_ ? $_ : ()} ($self->{reference}->{pages}->{start},$self->{reference}->{pages}->{stop}));
     }

}



1;


__END__






