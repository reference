# This module is part of da_reference, and is released
# under the terms of the GPL version 2, or any later version. See the
# file README and COPYING for more information.
# Copyright 2003 by Don Armstrong <don@donarmstrong.com>.
# $Id: Date.pm 42 2009-03-20 06:29:46Z don $

package Reference::Field::Date;

=head1 NAME

 --

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 BUGS

None known.

=cut


use strict;
use vars qw($REVISION $DEBUG);

use NEXT;
use Params::Validate qw(:types validate_with);
use Date::Manip;


BEGIN{
     ($REVISION) = q$LastChangedRevision: 42 $ =~ /\$LastChangedRevision:\s+([^\s+])/;
     $DEBUG = 0 unless defined $DEBUG;
}

=head2 date

     

XXX DOCUMENT ME

=cut


sub date{
     my $self = shift;
     my %params;
     if (scalar(@_) == 1) {
	  $params{date} = shift;
	  $params{output} = 'scalar';
     }
     else {
	  %params = validate_with(params => \@_,
				  spec   => {date  => {type     => ARRAYREF|SCALAR|HASHREF|UNDEF,
						       optional => 1,
						      },
					     day   => {type => SCALAR|UNDEF,
						       optional => 1,
						      },
					     year  => {type => SCALAR|UNDEF,
						       optional => 1,
						      },
					     month => {type => SCALAR|UNDEF,
						       optional => 1,
						      },
					     output => {default => 'scalar',
							type    => SCALAR,
						       },
					    },
				 );
     }
     # Update author according to the passed information
     if (defined $params{day} or defined $params{year} or defined $params{month}) {
	  $self->{reference}->{date}->{day}    = $params{day}   if defined $params{day};
	  $self->{reference}->{date}->{year}   = $params{year}  if defined $params{year};
	  $self->{reference}->{date}->{month}  = $params{month} if defined $params{month};
     }
     elsif (defined $params{date}) {
	  $self->{reference}->{date} = {day   => undef,
					year  => undef,
					month => undef,
				       };
	  my $date = ParseDate($params{date});
	  $self->{reference}->{date}->{unix} = $date;
	  ($self->{reference}->{date}->{day},
	   $self->{reference}->{date}->{year},
	   $self->{reference}->{date}->{month}) = UnixDate($date,qw(%e %Y %m));
     }

     local $_ = $params{output};
     if (/bibtex/) {
	  return UnixDate($self->{reference}->{date}->{unix},'%B %e %Y') if defined $self->{reference}->{date}->{unix};
	  return join(' ',$self->{reference}->{date}->{day},$self->{reference}->{date}->{year},$self->{reference}->{date}->{month});
     }
     elsif (/year/) {
	 return UnixDate($self->{reference}->{date}->{unix},'%Y') if defined $self->{reference}->{date}->{unix};
	 return $self->{reference}->{date}->{year};
     }
     else {
	  return UnixDate($self->{reference}->{date}->{unix},'%B %e %Y') if defined $self->{reference}->{date}->{unix};
	  return join(' ',$self->{reference}->{date}->{day},$self->{reference}->{date}->{year},$self->{reference}->{date}->{month});
     }
}

=head2 year

     

Returns the year associated with the date field


=cut


sub year{
     my $self = shift;

     return $self->{reference}->{date}->{year};
}

=head2 day

     

Returns the day associated with the date field

=cut

sub day{
     my $self = shift;

     return $self->{reference}->{date}->{day};
}

=head2 month

     

Returns the month associated with the date field

=cut

sub month{
     my $self = shift;

     return $self->{reference}->{date}->{month};
}


sub _init{
     my $self = shift;

     $self->{reference}->{date} = {month => undef,
				   year  => undef,
				   day   => undef,
				   unix  => undef,
				  };

     $self->NEXT::_init;

}




1;


__END__






