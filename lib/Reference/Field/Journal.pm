# This module is part of da_reference, and is released
# under the terms of the GPL version 2, or any later version. See the
# file README and COPYING for more information.
# Copyright 2003 by Don Armstrong <don@donarmstrong.com>.
# $Id: Journal.pm 44 2013-09-10 00:37:13Z don $

package Reference::Field::Journal;

=head1 NAME

 --

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 BUGS

None known.

=cut


use strict;
use vars qw($REVISION $DEBUG @JOURNAL_FIELDS);

use NEXT;
use Params::Validate qw(:types validate_with);

BEGIN{
     ($REVISION) = q$LastChangedRevision: 44 $ =~ /\$LastChangedRevision:\s+([^\s+])/;
     $DEBUG = 0 unless defined $DEBUG;
     @JOURNAL_FIELDS = qw(title medlineabbr isoabbr nlmid issn eissn publisher pmid);
}


sub _init{
     my $self = shift;

     $self->{reference}->{journal} = {};
     @{$self->{reference}->{journal}}{@JOURNAL_FIELDS} = (undef) x scalar @JOURNAL_FIELDS;

     $self->NEXT::_init;

}

sub journal{
     my $self = shift;
     my %params;
     if (scalar(@_) == 1) {
	  $params{journal} = shift;
	  $params{output} = 'scalar';
     }
     else {
	  my %spec;
	  @spec{@JOURNAL_FIELDS} = ({type => SCALAR|UNDEF,optional=>1}) x scalar @JOURNAL_FIELDS;
	  %params = validate_with(params => \@_,
				  spec   => {journal     => {type     => SCALAR,
							     optional => 1,
							    },
					     output      => {type     => SCALAR,
							     default  => 'scalar',
							    },
					     %spec,
					    },
				 );
     }
     # Were we called using $reference->journal(foo=>bar) {ignoring journal=>bar} ?
     my $using_param_call = 0;
     foreach my $key (@JOURNAL_FIELDS) {
	  $using_param_call = 1 and last if exists $params{$key} and defined $params{$key};
     }
     if ($using_param_call) {
	  foreach my $key (@JOURNAL_FIELDS) {
	       $self->{reference}->{journal}->{$key} = $params{$key} if exists $params{$key} and defined $params{$key};
	  }
     }
     elsif (defined $params{journal}) {
	  $self->{reference}->{journal}->{title} = $params{journal};
     }

     local $_ = $params{output};
     if (/bibtex/) {
         my $title = $self->{reference}->{journal}->{medlineabbr} || $self->{reference}->{journal}->{title};
         $title =~ s/[^A-Za-z0-9]//g;
         return $title;
     } elsif (/medline/) {
         return $self->{reference}->{journal}->{medlineabbr} || $self->{reference}->{journal}->{title};
     } elsif (/iso/) {
         return $self->{reference}->{journal}->{isoabbr} || $self->{reference}->{journal}->{title};
     }
     else {
	  return $self->{reference}->{journal}->{title};
     }
}



1;


__END__






