# This module is part of da_reference, and is released
# under the terms of the GPL version 2, or any later version. See the
# file README and COPYING for more information.
# Copyright 2003 by Don Armstrong <don@donarmstrong.com>.
# $Id: Miner.pm 30 2004-06-29 10:26:20Z don $

package ;

=head1 NAME

 --

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 BUGS

None known.

=cut


use strict;
use vars qw($REVISION $DEBUG);

BEGIN{
     ($REVISION) = q$LastChangedRevision: 30 $ =~ /\$LastChangedRevision:\s+([^\s+])/;
     $DEBUG = 0 unless defined $DEBUG;
}



1;


__END__






